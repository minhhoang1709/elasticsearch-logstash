![enter image description here](https://i0.wp.com/www.idfor-solutions.com/wp-content/uploads/2018/01/ELK.png?w=1080)
## Hướng dẫn đồng bộ Database với Elasticsearch
**Bước 1:** Tải và cài đặt elasticsearch, tích hợp tìm kiếm và phân tích tiếng Việt [tại đây](https://viblo.asia/p/elasticsearch-phan-tich-va-tim-kiem-du-lieu-tieng-viet-3P0lPveoKox)

Sử dụng chrome extension [ElasticSearch Head](https://chrome.google.com/webstore/detail/elasticsearch-head/ffmkiejjmecolpfloofpjologoblkegm) để thao tác elasticsearch

Tạo index trong elasticsearch bằng query trong file elasticsearch-user.txt

Trong Postgres sử dụng file activemq_data.sql để import database

**Bước 2:** Cài đặt Logstash, kiểm tra phiên bản phù hợp theo bảng dưới

| Elasticsearch | Logstasg |
|--|--|
| 2.0.x | 2.0.x-5.6.x |
| 2.1.x | 2.0.x-5.6.x |
| 2.2.x | 2.0.x-5.6.x |
| 2.3.x | 2.0.x-5.6.x |
| 2.4.x | 2.0.x-5.6.x |
| 5.0.x | 5.0.x-5.6.x |
| 5.1.x | 5.0.x-5.6.x |
| 5.2.x | 5.0.x-5.6.x |
| 5.3.x | 5.0.x-5.6.x |
| 5.4.x | 5.0.x-5.6.x |
| 5.5.x | 5.0.x-5.6.x |
| 5.6.x | 5.0.x-5.6.x |
| 6.0.x | 6.0.x-6.8.x |
| 6.1.x | 6.0.x-6.8.x |
| 6.2.x | 6.0.x-6.8.x |
| 6.3.x | 6.0.x-6.8.x |
| 6.4.x | 6.0.x-6.8.x |
| 6.5.x | 6.0.x-6.8.x |
| 6.6.x | 6.0.x-6.8.x |
| 6.7.x | 6.0.x-6.8.x |
| 6.8.x | 6.0.x-6.8.x |
| 7.0.x | 7.0.x-7.2.x |
| 7.1.x | 7.0.x-7.2.x |
| 7.2.x | 7.0.x-7.2.x |

Tải về Logstash [tại đây](https://www.elastic.co/fr/downloads/past-releases#logstash), giải nén.

**Bước 3:** Cài đặt plugin logstash-input-jdbc bằng cách chạy terminal trong thư mục logstash vừa giải nén

> bin/plugin install logstash-input-jdbc

**Bước 4:** Tải về thư viện jdbc tương ứng với database, ví dụ jdbc của Postgres [tại đây](https://jdbc.postgresql.org/download.html)

**Bước 5:** Tải và đặt file config và thư mục bin, chỉnh sửa file config theo database và elasticsearch muốn đồng bộ(properties của type trong index cần khớp với table trong database) , sau đó khởi chạy logstash

> bin/logstash -f logstash-config.conf

Dữ liệu từ elasticsearchsẽ được đồng bộ với database
